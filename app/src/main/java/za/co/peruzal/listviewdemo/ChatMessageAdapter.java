package za.co.peruzal.listviewdemo;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by joseph on 7/15/15.
 */
public class ChatMessageAdapter extends BaseAdapter {
    private ArrayList<ChatMessage> arrayList;
    private Context context;
    public ChatMessageAdapter(ArrayList<ChatMessage> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {

            rowView = View.inflate(context, R.layout.row_chatmessage,null);

            TextView username = (TextView) rowView.findViewById(R.id.username);
            TextView message = (TextView)rowView.findViewById(R.id.message);

            ChatMessage chatMessage = arrayList.get(position);

            username.setText(chatMessage.getUsername());
            message.setText(chatMessage.getMessage());

        return rowView;
    }
}

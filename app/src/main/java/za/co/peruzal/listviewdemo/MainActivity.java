package za.co.peruzal.listviewdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    ArrayList<ChatMessage> chatMessages;
    @Bind(R.id.listView) ListView listview;
    @Bind(R.id.chat) EditText chatBox;
   ChatMessageAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        chatMessages = new ArrayList<>();

        chatMessages.add(new ChatMessage("Joseph", "Howdy!"));
        chatMessages.add(new ChatMessage("Sasha", "How are you"));
        chatMessages.add(new ChatMessage("Armanda", "Bye"));
        chatMessages.add(new ChatMessage("Ralph", "Ninos"));

        adapter = new ChatMessageAdapter(chatMessages, this);
        listview.setAdapter(adapter);


    }

    @OnClick(R.id.btnSend) public void addMessagetoList(){

    }


}
